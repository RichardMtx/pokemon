import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { POKEMONS } from '../mock-pokemon-list';
import { Pokemon } from '../pokemon';

@Component({
  selector: 'app-detail-pokemon',
  templateUrl: './detail-pokemon.component.html',
  styleUrls: ['./detail-pokemon.component.scss']
})
export class DetailPokemonComponent implements OnInit {

  pokemonList: Pokemon[];
  pokemon: Pokemon|undefined;

  constructor(private route: ActivatedRoute, private router: Router) {  }

  ngOnInit() {
    this.pokemonList = POKEMONS;
    // Récupération à l'instant `T` du paramètre dans la route (ActivatedRoute) correspondant à `id`
    const pokemonId: string|null = this.route.snapshot.paramMap.get('id');
    // find() on cherche le pokemon qui a l'id que l'utilisateur à demander
    if(pokemonId)
      this.pokemon = this.pokemonList.find(pokemon => pokemon.id === +pokemonId);
  }

  goBack(){
    this.router.navigate(['/pokemons'])
  }

}
