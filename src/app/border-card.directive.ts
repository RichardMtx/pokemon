import { Directive, ElementRef, Host, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[pokemonBorderCard]'
})
export class BorderCardDirective {

  private initialColor: string = "#E0E1E9";
  private defaultColor: string = "#66B3BA";
  private defaultHeight: number = 180;

  constructor(private _element: ElementRef) {
    this.setHeight(this.defaultHeight);
    this.setBorder(this.initialColor);
  }

  @Input('pokemonBorderCard') borderColor: string; // alias

  @HostListener('mouseenter') onMouseEnter() {
    this.setBorder(this.borderColor || this.defaultColor)
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.setBorder(this.initialColor)
  }

  setHeight(height: number) {
    this._element.nativeElement.style.heigh = `${height}px`;
  }

  setBorder(color: string) {
    this._element.nativeElement.style.border = `solid 2px ${color}`;
  }
}