# Dockerfile
FROM node:lts
RUN mkdir /home/node/pokemon && chown -R node:node /home/node/pokemon
RUN mkdir /home/node/pokemon/node_modules && chown -R node:node /home/node/pokemon/node_modules
WORKDIR  /home/node/pokemon
USER node
COPY --chown=node:node package.json package-lock.json ./
RUN npm ci --quiet
COPY --chown=node:node . .